package restaum;

public class Movimento {
	int[][] movimento;
	String direcao;
	
	public Movimento(int[][] movimento, String direcao) {
		this.movimento = movimento;
		this.direcao = direcao;
	}
	
	public int[][] getMovimento() {
		return movimento;
	}

	public void setMovimento(int[][] movimento) {
		this.movimento = movimento;
	}

	public String getDirecao() {
		return direcao;
	}

	public void setDirecao(String direcao) {
		this.direcao = direcao;
	}

	@Override
	public String toString() {
		return ("[LINHA: "+movimento[0][0] + " COLUNA: " + movimento[0][1]) +", DIREÇÃO: " + direcao+"]";
	}
	
}
