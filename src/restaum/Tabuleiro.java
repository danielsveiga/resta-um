package restaum;

public class Tabuleiro {
	
	int[][] tabuleiro;
	private int pecas = 32;
	private String[] DIRECOES = {"CIMA", "DIREITA", "BAIXO", "ESQUERDA"};
    
    public Tabuleiro() {
        this.tabuleiro = new int[7][7];
    }
    
    //Inicializa tabuleiro com a posições com peças, sem peças e não usáveis.
    public void inicializaTabuleiro() {
        for(int i=0; i<7; i++) {
            for(int j=0; j<7; j++) {
                if((i==0 && j==0) || (i==0 && j==1) || (i==1 && j==0) || (i==1 && j==1)
                        || (i==5 && j==0) || (i==5 && j==1) || (i==6 && j==0) || (i==6 && j==1)
                        || (i==0 && j==5) || (i==1 && j==5) || (i==0 && j==6) || (i==1 && j==6)
                        || (i==5 && j==5) || (i==5 && j==6) || (i==6 && j==5) || (i==6 && j==6)) {
                    this.tabuleiro[i][j] = 2;
                } else if(i==3 && j==3){
                    this.tabuleiro[i][j] = 0;
                } else {
                    this.tabuleiro[i][j] = 1;
                }
            }
        }
    }

    public int[][] getTabuleiro() {
        return tabuleiro;
    }

    public void setTabuleiro(int[][] tabuleiro) {
        this.tabuleiro = tabuleiro;
    }
    
    public int getPecas() {
    	return this.pecas;
    }
    
    public void setPecas(int pecas) {
    	this.pecas = pecas;
    }
    
    public int getHeight() {
    	return this.tabuleiro.length;
    }
    
    public int getLength() {
    	return this.tabuleiro[this.getHeight()-1].length;
    }
    
    public String[] getDirecoes() {
    	return this.DIRECOES;
    }
    
}
