package restaum;

import java.util.HashMap;
import java.util.Map;

public class RestaUm {
	/**
     * @param args the command line arguments
     */
	
	private static Map<Integer, Movimento> movimentos;
	private static int index = 0;
	
    public static void main(String[] args) {
        // TODO code application logic here
        Tabuleiro tabuleiro = new Tabuleiro();
        tabuleiro.inicializaTabuleiro();
        movimentos = new HashMap<>();
        
        imprimeTabuleiro(tabuleiro);
        
        System.out.println("Resolvendo... ");
        
        if (!acharSolucao(tabuleiro)) {
        	System.out.println("\nSolução não foi encontrada!");
        } else {
        	for(int i=movimentos.size()-1; i>=0; i--) {
            	System.out.println(movimentos.get(i));
            }
        	
        	System.out.println("");
        	imprimeTabuleiro(tabuleiro);
        }
        
//        imprimeTabuleiro(tabuleiro);
        
        
    }
    
    public static boolean acharSolucao(Tabuleiro tabuleiro) {
    	if(ehSolucao(tabuleiro)) {
    		System.out.println("Resolvido, solução encontrada com "+movimentos.size()+" movimentos:");
    		return true;
    	} else {
    		for(int i = 0; i < tabuleiro.getHeight(); i++) {
    			for(int j = 0; j < tabuleiro.getLength(); j++) {
    				for(int k = 0; k < tabuleiro.getDirecoes().length; k++) {
    					Movimento movimento = setaMovimento(j, i, tabuleiro.getDirecoes()[k]);
    					if (movimentoPossivel(tabuleiro, movimento)) {
    						movimentos.put(index, movimento);
    						executaMovimento(tabuleiro, movimento);
    						index++;
    						if (acharSolucao(tabuleiro)) {
    							return true;
			            	}
    						movimentos.remove(--index);
							desfazMovimento(tabuleiro, movimento);
			            }
    				}
    			}
    		}
    		return false;
    	}
    }
    
    private static Movimento setaMovimento(int linha, int coluna, String direcao) {
		int[][] movimento = new int[3][2];
		movimento[0][0] = linha;
		movimento[0][1] = coluna;
		
		switch (direcao) {

		case "BAIXO":
			movimento[1][0] = linha+1;
			movimento[1][1] = coluna;
			movimento[2][0] = linha+2;
			movimento[2][1] = coluna;
			break;
			
		case "ESQUERDA":
			movimento[1][0] = linha;
			movimento[1][1] = coluna-1;
			movimento[2][0] = linha;
			movimento[2][1] = coluna-2;
			break;
			
		case "DIREITA":
			movimento[1][0] = linha;
			movimento[1][1] = coluna+1;
			movimento[2][0] = linha;
			movimento[2][1] = coluna+2;
			break;
			
		case "CIMA":
			movimento[1][0] = linha-1;
			movimento[1][1] = coluna;
			movimento[2][0] = linha-2;
			movimento[2][1] = coluna;
			break;

		default:
			
			break;
		}
		
		return new Movimento(movimento, direcao);
	}
    
    private static void executaMovimento(Tabuleiro tabuleiro, Movimento movimento) {
		tabuleiro.getTabuleiro()[movimento.getMovimento()[0][0]][movimento.getMovimento()[0][1]] = 0;
		tabuleiro.getTabuleiro()[movimento.getMovimento()[1][0]][movimento.getMovimento()[1][1]] = 0;
		tabuleiro.getTabuleiro()[movimento.getMovimento()[2][0]][movimento.getMovimento()[2][1]] = 1;
		
		tabuleiro.setPecas(tabuleiro.getPecas()-1);
	}
    
    private static void desfazMovimento (Tabuleiro tabuleiro, Movimento movimento) {
		//System.out.println("undo");
		
    	tabuleiro.getTabuleiro()[movimento.getMovimento()[0][0]][movimento.getMovimento()[0][1]] = 1;
    	tabuleiro.getTabuleiro()[movimento.getMovimento()[1][0]][movimento.getMovimento()[1][1]] = 1;
    	tabuleiro.getTabuleiro()[movimento.getMovimento()[2][0]][movimento.getMovimento()[2][1]] = 0;
		
    	tabuleiro.setPecas(tabuleiro.getPecas()+1);
	}
    
    private static boolean movimentoPossivel(Tabuleiro tabuleiro, Movimento movimento) {
		
		if (movimento.getMovimento()[2][0] >= 7 || movimento.getMovimento()[2][1] >= 7 
				|| movimento.getMovimento()[2][0] < 0 || movimento.getMovimento()[2][1] < 0)
			return false;
		
		return 	(tabuleiro.getTabuleiro()[movimento.getMovimento()[0][0]][movimento.getMovimento()[0][1]] == 1) &&
				(tabuleiro.getTabuleiro()[movimento.getMovimento()[1][0]][movimento.getMovimento()[1][1]] == 1) &&
				(tabuleiro.getTabuleiro()[movimento.getMovimento()[2][0]][movimento.getMovimento()[2][1]] == 0) ;
	}
	
	private static void imprimeTabuleiro(Tabuleiro tabuleiro) {
		
		for (int i = 0; i < tabuleiro.getHeight(); i++) {
			for (int j = 0; j < tabuleiro.getLength(); j++) {
				System.out.print(tabuleiro.getTabuleiro()[i][j] + " ");
			}
			
			System.out.println();
		}
		
		System.out.println();
		
	}
    
    public static boolean ehSolucao(Tabuleiro tabuleiro) {
    	if(tabuleiro.getPecas() == 1 && tabuleiro.getTabuleiro()[3][3] == 1) {
    		return true;
    	} else {
    		return false;
    	}
    }
}
